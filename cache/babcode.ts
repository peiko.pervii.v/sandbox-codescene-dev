// import { Component, EventEmitter, OnInit, Input, Output, OnChanges, OnDestroy } from '@angular/core';
// import { UserService, HintService, SecretService } from './services';
// import { Logger } from './logger';

// @Component({
//   selector: 'app-example',
//   templateUrl: './xemaple.component.html',
//   styleUrls: ['./example.component.scss']
// })
// export class ExampleComponent implements OnInit {
//   @Input() title: string;
//   @Output() onXSelect = new EventEmitter<boolean>();
//   selectedUser: User;
//   userId: number;

//   config: any;
//   configData: any;

//   lang: string;
//   timezone: String;
//   hasGoodResolution: boolean;

//   selectX(): void {
//     const emittedUser: any = this.selectedUser
//     emittedUser.id = this.id;
//     this.onXSelect.emit(emittedUser);
//   }

//   constructor(userService: UserService, private macs: MyAppConfigService, private hintService: HintService, private logger: Logger, public secretService: SecretService) {
//     this.config = this.macs.subscribe(result => {
//       this.configData = result;
//     });

//     this.title = 1337;

//     this.initStuff();
//   }

//   initStuff(): void {
//     if (this.isThisGood()) {
//     this.title = 'Good Component';
//     } else {
//     this.title = 'Bad Component';
//     }

//     if (!this.configData.timeZone) {
//       this.timezone = "UTC";
//     } else {
//       this.timezone = this.configData.timeZone;
//     }

//     if (this.configData.language) {
//       this.lang = this.configData.language;
//     } else {
//       this.lang = 'en';
//     }

//     let width = window.screen.width;

//     if (width > 1000) {
//       this.hasGoodResolution = true;
//     } else {
//       this.hasGoodResolution = false;
//     }

//   }

//   getUser(): void {
//     var me = this;
//     this.userService.getUser().then(function (user) {
//       this.selectedUser = user;
//       me.userId = user.id;
//     });
//   }

//   isThisGood() {
//     return false;
//   }

// }

// interface User {
//   id: number,
//   username: any;
//   email;
// }

import {
    BadRequestException,
    CACHE_MANAGER,
    Inject,
    Injectable,
  } from '@nestjs/common';
  import Binance from 'node-binance-api';
  import { ExchangeRatesService as RatesService } from '@common/models/exchange-rates/exchange-rates.service';
  import { CurrencyService } from '@common/models/currency/currency.service';
  import { BinancePairService } from '@common/models/binance-pair/binance-pair.service';
  import { FePairService } from '@common/models/fe-pair/fe-pair.service';
  import { CryptoService } from '@common/models/crypto/crypto.service';
  import { OrdersService } from '@common/models/orders/orders.service';
  import axios from 'axios';
  import { Decimal } from 'decimal.js';
  import { Cache } from 'cache-manager';
  import { Interval } from '@nestjs/schedule';
  import { EXCHANGE_RATES_MODULE_OPTIONS } from './constants';
  import { ExchangeRatesModuleOptions } from './exchange-rates-module.interface';
  
  @Injectable()
  export class ExchangeRatesService {
    constructor(
      private orderService: OrdersService,
      private cryptoService: CryptoService,
      private fePairService: FePairService,
      private binancePairService: BinancePairService,
      private currencyService: CurrencyService,
      private exchangeRateService: RatesService,
      private binance: Binance,
      @Inject(EXCHANGE_RATES_MODULE_OPTIONS)
      private options: ExchangeRatesModuleOptions,
      @Inject(CACHE_MANAGER)
      private cacheManager: Cache,
    ) {}
  
    @Interval(30000)
    async get(): Promise<void> {
      const coinMarketCapRates = await this.coinMarketCap();
      try {
        const currencyList = await this.currencyService.currencyList(true);
        const fePairList = await this.fePairService.getListOfBinancePairs();
        const cryptoCoins = await this.cryptoService.getCoins();
        const fiatRates = await this.getFiatRates();
  
        return await this.binance.prices().then(async data => {
          // TODO: fix element implicitly has an any
          const exchangeData: Record<string, string | number> = {};
          cryptoCoins.forEach(c => {
            cryptoCoins.forEach(i => {
              if (i.symbol !== c.symbol) {
                const pairOne = i.symbol + c.symbol;
                if (data[pairOne]) {
                  exchangeData[pairOne] = data[pairOne];
                }
                const pairTwo = c.symbol + i.symbol;
                if (data[pairTwo]) {
                  exchangeData[pairTwo] = data[pairTwo];
                }
              }
            });
          });
          currencyList.map(i =>
            cryptoCoins.forEach(c => {
              if (data[c.symbol + i.bCode]) {
                exchangeData[c.symbol + i.code] = data[c.symbol + i.bCode];
              }
              if (data[i.bCode + c.symbol]) {
                exchangeData[i.code + c.symbol] = data[i.bCode + c.symbol];
              }
            }),
          );
          const tonId = ExchangeRatesService.idToSymbol()['TON'];
          const tonRate = coinMarketCapRates[tonId].quote['USD'].price; // 1 ton = 2usd
          const usdtTon = 1 / tonRate;
          cryptoCoins.forEach(c => {
            if (c.symbol === 'USDT') {
              exchangeData['TONUSDT'] = tonRate;
              exchangeData['TONUSD'] = tonRate;
            } else if (c.symbol === 'TON') {
            } else {
              exchangeData[c.symbol + 'TON'] =
                exchangeData[c.symbol + 'USDT'] * usdtTon;
            }
          });
          fePairList.forEach(pair => {
            if (data[pair]) {
              exchangeData[pair] = data[pair];
            }
          });
          currencyList.map(fiat =>
            cryptoCoins.forEach(crypto => {
              if (
                !(
                  exchangeData[crypto.symbol + fiat.code] ||
                  exchangeData[fiat.code + crypto.symbol]
                )
              ) {
                try {
                  exchangeData[crypto.symbol + fiat.code] = this.getRate(
                    exchangeData,
                    fiatRates,
                    fiat.code,
                    crypto.symbol,
                  );
                } catch (e: any) {
                  console.log(e.message);
                }
              }
            }),
          );
          await this.binancePairService.updatePairsList(Object.keys(data));
          await this.fePairService.sendUpdatedCourse(exchangeData);
          await this.exchangeRateService.save(exchangeData);
          await this.orderService.closeOrderByVolatility(exchangeData);
        });
      } catch (err) {
        console.log('err', err);
      }
    }
  
    private async getFiatRates() {
      const exist = await this.cacheManager.get('fiatRates');
      if (exist) {
        return exist;
      }
      const rates = await axios
        .get('https://api.exchangerate.host/latest?base=USD')
        .then(res => res.data?.rates);
      await this.cacheManager.set('fiatRates', rates, 360);
      return rates;
    }
  
    // TODO: fix element implicitly has an any
    private getRate(exchangeData, fiatRates, fiatCode, cryptoCode): number {
      let rate: Decimal;
      if (exchangeData[cryptoCode + 'USD']) {
        rate = new Decimal(exchangeData[cryptoCode + 'USD']);
      } else if (exchangeData['USD' + cryptoCode]) {
        rate = new Decimal(1).div(exchangeData['USD' + cryptoCode]);
      } else {
        throw new BadRequestException(
          'Rate not found ' + fiatCode + ' ' + cryptoCode,
        );
      }
  
      const fiatRate = fiatRates[fiatCode] || null;
      if (!fiatRate) {
        throw new BadRequestException('Fiat rate not found');
      }
  
      return rate.times(fiatRate).toDecimalPlaces(8).toNumber();
    }
  
    private async coinMarketCap() {
      const exist = await this.cacheManager.get('coinMarketCapRates');
      if (exist) {
        return exist;
      }
      const {
        data: { data: rates },
      } = await axios.get(
        `https://pro-api.coinmarketcap.com/v2/cryptocurrency/quotes/latest`,
        {
          params: {
            id: '1,1027,825,1958,11419',
            CMC_PRO_API_KEY: this.options.coinMarketCap.key,
          },
        },
      );
      await this.cacheManager.set('coinMarketCapRates', rates, 360);
  
      return rates;
    }
  
    private static idToSymbol() {
      return {
        BTC: 1,
        ETH: 1027,
        USDT: 825,
        TRX: 1958,
        TON: 11419,
      };
    }
  }
  